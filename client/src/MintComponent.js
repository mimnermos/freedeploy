import React, { Component } from "react";
import URIMinterContract from "./contracts/WeMintArt.json";
import WeMintArtOld from "./contracts/WeMintArtOld.json";
import ERC165 from "./contracts/ERC165.json";
import MintFormComponent from "./MintFormComponent";
import {CircularProgress,Grid} from '@mui/material'

import "./App.css";
import { CButton } from "./components/CButton";
import { CInput } from "./components/CInput";
import { Message } from "./components/Message";
import { Box } from "@mui/system";

class MintComponent extends Component {
    
  state = { 
      isLoading: false, 
      isLoaded: false, 
      contractAddress: '', 
      contract: null,
      contractError: false,
  };

  componentDidMount = async () => {
      this.handleContractAddressChange = this.handleContractAddressChange.bind(this);
      this.handleLoadContract = this.handleLoadContract.bind(this);
  }

  handleContractAddressChange = (event) => {
    this.setState({ contractAddress: event.target.value, contractError: !event.target.value || !/^0x([a-fA-F0-9]){40}$/g.test(event.target.value) }); 
  }

  handleLoadContract = async (event) => {
      event.preventDefault();
      if(!this.state.contractAddress){
            this.setState({contractError: !this.state.contractAddress}); 
          return;
      }
      this.setState({isLoading: true});

      const networkId = this.props.web3;
      //const deployedNetwork = URIMinterContract.networks[networkId];
      const erc165 = new this.props.web3.eth.Contract(
        ERC165.abi,
        this.state.contractAddress
      );
      var instance;
      const hasRoyalties = await erc165.methods.supportsInterface("0x2a55205a").call();
      console.log("hasRoyalties = "+hasRoyalties);
      this.setState({hasRoyalties: hasRoyalties});
      if(hasRoyalties){
        instance = new this.props.web3.eth.Contract(
            URIMinterContract.abi,
            this.state.contractAddress
        );
      }else{
          instance = new this.props.web3.eth.Contract(
            WeMintArtOld.abi,
            this.state.contractAddress
          );
      }

      this.setState({contract: instance});

      this.setState({isLoaded:true});
      this.setState({isLoading: false});
  }

  clear = () => {
    this.setState({
      isLoading: false, 
      isLoaded: false, 
      contractAddress: '', 
      contract: null,
      contractError: false,
    })
  }

  render() {

    const isWalletConnected = this.props.accounts && this.props.accounts.length

      return(
            <Grid container spacing ={3}>
            <Grid item xs={12} md={4}>
            {/* Contract load input form */}
            {!this.state.isLoaded && (
              <div>
                <Box as='form' onSubmit={this.handleLoadContract} sx={{ mt: 3 }}>
                    <CInput
                      sx={{ mb: 4 }}
                      error={this.state.contractError} 
                      helperText={this.state.contractError? "A valid contract address is required to mint" : ""}
                      label="Contract Address"
                      value={this.state.contractAddress}
                      onChange={this.handleContractAddressChange}
                    />
                    <br/>
                    {this.state.isLoading && <CircularProgress style={{color:'black', marginLeft:'12%'}} />}
                    <div>
                        {!this.state.loading && <CButton sx={{ mb: 1 }} variant='alternative'  disabled={!isWalletConnected || this.state.contractError || this.props.chainError } type='submit'>Load</CButton>}
                        {
                          !isWalletConnected && 
                          (
                            <Box sx={{ mt: 3 }}>
                              <Message title='Connect wallet to mint!!' variant='error' />
                            </Box>
                          )
                        }
                        {
                          this.props.chainError && 
                          (
                            <Box sx={{ mt: 3 }}>
                              <Message title='Switch to Ethereum testnet to mint :)' variant='error' />
                            </Box>
                          )
                        }
                    </div>
                </Box>
              </div>
            )}
            {/* Mint input form */}
            {
              this.state.isLoaded && (
                <Box sx={{ wordBreak: 'break-all'}}>
                  <p>Contract <em>{this.state.contract._address}</em> loaded</p>
                  <MintFormComponent 
                    contract={this.state.contract} 
                    accounts={this.props.accounts}
                    chainError={this.props.chainError}
                    hasRoyalties={this.state.hasRoyalties}
                    onMintSuccess={() => {
                      this.clear()
                    }}
                  />
                </Box>
              )
            }
            </Grid>
            <Grid item xs={12} md={8}>
              <p>
          Mint your 1/1 NFT here!<span>{String.fromCodePoint("0x1F3A8")}</span> Upload your art and metadata to IPFS first then enter the details here after. You can add your preferred royalties too, if your contract includes the EIP-2981 update. For more info <a href="https://gitlab.com/mimnermos/freemint/-/wikis/home"> read the docs</a>.
              </p> 
            </Grid>
            </Grid>
      );
  }
}


export default MintComponent;
