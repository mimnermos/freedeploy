import React from 'react';
import * as Images from './ImageExtractor';

import { Box, maxHeight, maxWidth, minWidth } from '@mui/system';
import Icon from '@mui/icons-material/ArrowForward'
import requirePropFactory from '@mui/utils/requirePropFactory';


const imageStyle={
    marginTop: 40,
    marginLeft:35,
    width:'90%',
}


const AboutComponent = (props) => {
  return (
      <Box color='black' fontFamily='Menlo'  >
        <Box fontSize={23}>
          HOW TO DEPLOY
        </Box>

        <Box  display='flex' flexDirection='row'  marginTop={2} marginLeft={2} fontSize={13}>
        <span>1)</span>
        <span style={{marginLeft:8}}>
        Click on the ‘Deploy’ tab on wemint.art 
        </span>
        </Box>

        <Box  display='flex' flexDirection='row'  marginTop={2} marginLeft={2} fontSize={13}>
        <span>2)</span>
        <span style={{marginLeft:8}}>
        Choose your own ticker and contract name, eg: 'ART' and 'My Art Contract'. 
        Choose the names carefully as they
        will be on the Ethereum blockchain forever.
        </span>
        </Box>

        <Box  display='flex' flexDirection='row'  marginTop={2} marginLeft={2} fontSize={13}>
        <span>3)</span>
        <span style={{marginLeft:8}}>
        Click ‘Deploy!’. 
        </span>
        </Box>


        <Box  display='flex' flexDirection='row'  marginTop={2} marginLeft={2} fontSize={13}>
        <span>4)</span>
        <span style={{marginLeft:8}}>
        Metamask should bring up a window telling you the gas fees to deploy. 
        If gas is too high for you,
        wait and keep checking <a href="https://etherscan.io/gastracker" target="_blank">etherscan.io gas tracker</a> for gas prices to drop.
        </span>
        </Box>
        

        <Box  display='flex' flexDirection='row'  marginTop={2} marginLeft={2} fontSize={13}>
        <span>5)</span>
        <span style={{marginLeft:8}}>
    Once you've deployed, you can check your contract by going to <a href="https://etherscan.io" target="_blank">etherscan.io</a> (or <a href="https://rinkeby.etherscan.io" target="_blank">rinkeby.etherscan.io</a> if testing on testnet) and entering your wallet address. You should see the contract creation hash and all the details under your profile.
        </span>
        </Box>

        

        <Box  display='flex' flexDirection='row'  marginTop={2} marginLeft={2} fontSize={13}>
        <span>6)</span>
        <span style={{marginLeft:8}}>
          You have now deployed your own custom ERC721 contract on the blockchain!
        </span>
        </Box>

        <Box fontSize={23} marginTop={3}>
          HOW TO MINT
        </Box>

        <Box  display='flex' flexDirection='row'  marginTop={2} marginLeft={2} fontSize={13}>
        <span style={{marginLeft:4}}>
        Here we describe the easiest and quickest way to mint your NFT.
        </span>
        </Box>

        <Box  display='flex' flexDirection='row'  marginTop={2} marginLeft={4} fontSize={13}>
        <span>1)</span>
        <span style={{marginLeft:8}}>
        Create an account on <a href="https://www.pinata.cloud/" target="_blank">pinata.cloud</a>
        </span>
        </Box>

        <Box>
          <Box display='flex' flexDirection='row' alignItems='center' marginTop={2} marginLeft={4} fontSize={13}>
            <span>2)</span>
            <span style={{marginLeft:8}}>
            Upload your artwork to IPFS on Pinata, by clicking 'Upload' then 'File'.
            </span>
          </Box>
        <img style={{ ...imageStyle, width: '40%' }} src={Images.Image1} />
        </Box>


        <Box>
          <Box display='flex' flexDirection='row'  marginTop={2} marginLeft={4} fontSize={13}>
            <span>3)</span>
            <span style={{marginLeft:8}}>
            Wait for your artwork to go through the pinning queue.  
            Depending on the file size, it can take some time
            for it to pin.
            Once it's pinned, it'll appear in the list marked 'Pinned'. Your artwork is now on IPFS.
            </span>
          </Box>
        <img style={imageStyle} src={Images.Image2} />
        </Box>


        <Box>
          <Box display='flex' flexDirection='row'  marginTop={2} marginLeft={4} fontSize={13}>
            <span>4)</span>
            <span style={{marginLeft:8}}>
            Now you need to create your metadata file.  
            Download this <a href='/yourartwork.json' download>template</a> and use a text editor to edit the name and 
            description, without modifying any of the surrounding code.
            This is a basic template. To learn more about metadata, 
          read <a target="_blank" href="https://docs.opensea.io/docs/metadata-standards">Opensea's</a> Guidelines.
            </span>
          </Box>
        <img style={imageStyle} src={Images.Image3} />
        </Box>

        <Box>
          <Box display='flex' flexDirection='row'  marginTop={2} marginLeft={4} fontSize={13}>
            <span>5)</span>
            <span style={{marginLeft:8}}>
            Copy the CID of your artwork from Pinata by clicking the 'Copy' button.
            </span>
          </Box>
        <img style={imageStyle} src={Images.Image4} />
        </Box>

        <Box>
          <Box display='flex' flexDirection='row'  marginTop={2} marginLeft={4} fontSize={13}>
            <span>6)</span>
            <span style={{marginLeft:8}}>
              Paste it into your metadata .json file, after the prefix 'ipfs://'.
            </span>
          </Box>
        <img style={imageStyle} src={Images.Image5} />
        </Box>


        <Box  display='flex' flexDirection='row' marginTop={2} marginLeft={4} fontSize={13}>
        <span>7)</span>
        <span style={{marginLeft:8}} >
          Save your metadata file with a name of your choice, then upload your metadata file to Pinata the same way you did
          your artwork.
        </span>
        </Box>


        <Box>
          <Box display='flex' flexDirection='row'  marginTop={2} marginLeft={4} fontSize={13}>
            <span>8)</span>
            <span style={{marginLeft:8}}>
            You should now see both your artwork and metadata files uploaded and pinned on Pinata.
            Your artwork and metadata are now both on IPFS. You're ready to mint!
            </span>
          </Box>
        <img style={imageStyle} src={Images.Image6} />
        </Box>


        <Box  display='flex' flexDirection='row'  marginTop={2} marginLeft={4} fontSize={13}>
        <span>9)</span>
        <span style={{marginLeft:8}}>
        On wemint.art, click the 'Mint' tab.
        </span>
        </Box>

        <Box  display='flex' flexDirection='row'  marginTop={2} marginLeft={4} fontSize={13}>
        <span>10)</span>
        <span style={{marginLeft:8}}>
        Enter your contract address. You can find it on <a href="https://etherscan.io" target="_blank">etherscan.io</a>, as described above.

        </span>
        </Box>

        <Box  display='flex' flexDirection='row'  marginTop={2} marginLeft={4} fontSize={13}>
        <span>11)</span>
        <span style={{marginLeft:8}}>
          Click 'Load'.
        </span>
        </Box>


        <Box>
          <Box display='flex' flexDirection='row'  marginTop={2} marginLeft={4} fontSize={13}>
            <span>12)</span>
            <span style={{marginLeft:8}}>
              Enter your metadata URL on wemint.art. This is the CID of your metadata file plus the prefix 'ipfs://'.
              You can get the metadata CID from your Pinata page.
              Remember it's not the CID of the image file, it's the CID of the metadata file!
            </span>
          </Box>
        <img style={imageStyle} src={Images.Image7} />
        </Box>

        <Box  display='flex' flexDirection='row'  marginTop={2} marginLeft={4} fontSize={13}>
        <span>13)</span>
        <span style={{marginLeft:8}}>
        Enter your preferred royalties if your contract includes the EIP-2981 update. If your contract doesn’t include EIP-2981, you won’t see this option. Remember that EIP-2981 isn’t accepted by the major NFT platforms so it won’t make any difference yet! For now, if you sell on Opensea for example, you can set your royalties there when you swap. 
        </span>
        </Box>

        <Box  display='flex' flexDirection='row'  marginTop={2} marginLeft={4} fontSize={13}>
        <span>14)</span>
        <span style={{marginLeft:8}}>
          Click 'Mint!'
        </span>
        </Box>

        <Box  display='flex' flexDirection='row'  marginTop={2} marginLeft={4} fontSize={13}>
        <span>15)</span>
        <span style={{marginLeft:8}}>
    Wait a bit. Depending on the file size, it can take a couple of hours before you see your art on Opensea and Rarible. You may see a different placeholder image while your NFT is loading (eg: your Opensea profile pic). Don't worry, it just takes time!

        </span>
        </Box>

        <Box  display='flex' flexDirection='row'  marginTop={2} marginLeft={4} fontSize={13}>
        <span>15)</span>
        <span style={{marginLeft:8}}>
    Your artwork is now minted and you can list it for sale!
        </span>
        </Box>

      </Box>
      );
}
export default AboutComponent;
