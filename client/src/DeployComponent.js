import React, { useState } from "react";
import {makeStyles} from '@mui/styles';
import { CircularProgress, Grid } from '@mui/material';
import URIMinterContract from "./contracts/WeMintArt.json";
import getWeb3 from "./getWeb3";
import { rawEncode } from "ethereumjs-abi";
import sourceCodeRaw from './WeMint.sol';
import "./App.css";
import { Box } from "@mui/system";
import { CButton } from "./components/CButton";
import { CInput } from "./components/CInput";
import { Message } from "./components/Message";
import { CModal } from "./components/CModal";

const DeployComponent = (props) => {
    console.log("DeployComponent: chainError = "+props.chainError);
    const [modal,setModal] = useState(false);
    const classes = useStyles();
    const [ticker,setTicker] = useState({
        tickerName:'',
        tickerError:false,
    })
    const [contract,setContract] = useState({
        contractName:'',
        error:false,
        contractAddress:null,

    })
    const [state, setState] = useState({ 
        isDeploying: false, 
        isVerifying: false, 
    });
    const [transactionHash, setHash] = useState({
        hash: null
    });
    const [deployError, setDeployError] = useState({
        deployError: false
    });


    const handleContractNameChange = (event) => {
        let regex = /^[a-zA-Z0-9&%$#@\*?!]+/g;
        setContract({contractName:event.target.value,error:!event.target.value || !regex.test(event.target.value)})
    }
    const handleTickerChange = (event) => {
        let regex = /^[a-zA-Z0-9&%$#@\*?!]+/g;
        setTicker({tickerName:event.target.value,tickerError:!event.target.value || !regex.test(event.target.value)})
    }

    const handleSubmit = async (event) => {
        console.log('handle submit')
        event.preventDefault();
        setDeployError({deployError: false})
        if(!ticker.tickerName || !contract.contractName){
            setTicker({tickerError:!ticker.tickerError})
            setContract({error:!contract.error})
            return;
        }
        setState({isDeploying:true});
        try {
            let abi = URIMinterContract.abi;
            let code = URIMinterContract.bytecode;
            let URIMinter = new props.web3.eth.Contract(abi);
            console.log("Deploying the contract");
            URIMinter.deploy({data:code, arguments: [contract.contractName, ticker.tickerName]}).send({
                from: props.accounts[0],
            })
                .on('transactionHash', function(transactionHash){ 
                    setHash({hash: transactionHash}); 
                })
                .then((deployment) => {
                    console.log('Contract was deployed at the following address:');
                    let contractAddress = deployment.options.address;
                    setContract({...contract,contractAddress: contractAddress})
                    etherscanVerify(contractAddress);
                    setModal(true)
                    setState({isDeploying:false})
                    // setState({...state, isDeploying: false, contractAddress: contractAddress, ticker: '', contractName: '' });
            }).catch((err) => {
                console.error(err);
                // setState({...state, isDeploying: false});
                setState({isDeploying:false})
                setDeployError({deployError:true})
            });
        } catch (error) {
            console.error(error);
            setState({ isDeploying: false});
        }
    }

    const etherscanVerify = async (addr) => {
        // setState({...state, isVerifying: true});
        setState({isVerifying:true})
        fetch(sourceCodeRaw)
            .then(source => source.text())
            .then(text => {
                let argTicker = props.web3.eth.abi.encodeParameter("string memory", ticker.tickerName);
                let argName = props.web3.eth.abi.encodeParameter("string memory", contract.contractName);
                let data = {
                    APIKey: '8T9N19JZDJZP7CIP7RY7HFV7N4RGS9PN7A',
                    module: 'contract',
                    action: 'verifysourcecode',
                    contractaddress: addr,
                    sourceCode: text,
                    codeformat: 'solidity-single-file',
                    contractname: contract.contractName,
                    EVMVersion: 'default',
                    compilerversion: 'v0.8.0+commit.c7dfd78e',
                    runs: 200,
                    constructorArguments: [argTicker, argName],
                    licenseType: 1
                }; 
                fetch('https://api-rinkeby.etherscan.io/api', {
                    method: "POST",
                    mode: "cors",
                    body: JSON.stringify(data),
                    headers: {"Content-type": "application/json; charset=UTF-8"}
                })
                .then(success => {
                    console.log("verified!");
                    console.log("Address: "+contract.contractAddress)
                    setState({isVerifying:false});
                })
                .catch(error => {
                    console.log(error);
                    setState({isVerifying:false});
                });
            });
    }


    const isWalletConnected = !!props.accounts && !!props.accounts.length
    // const buttonError = state.tickerError || state.nameError;
    

    const isFormComplete = ticker.tickerName?.length > 0 && contract.contractName?.length > 0

    return (
      <Box >
        <Grid container direction='row' spacing={3}>
        <Grid item xs={12} md={4}>
            <form onSubmit={handleSubmit} style={{marginTop:3}}>
                <CInput
                    sx={{ display: 'block', mb: 3 }}
                    label='Your Ticker'
                    value={ticker.tickerName}
                    error={ticker.tickerError} 
                    helperText={ticker.tickerError? "A valid ticker is required to deploy" : ""} 
                    onChange={handleTickerChange}
                />
                <CInput
                    label='Contract Name'
                    value={contract.contractName}
                    error={contract.error}
                    helperText={contract.error? "A valid contract name is required to deploy" : ""}
                    onChange={handleContractNameChange}
                />
                <Box sx={{ mt: 4 }}>
                    { (state.isVerifying || state.isDeploying) && <CircularProgress sx={{ color:'black', ml: 4, mb: 4 }} /> }
                    {!state.isVerifying && !state.isDeploying && <CButton sx={{ mb: 4, display: 'block' }} disabled={!isWalletConnected || !isFormComplete || props.chainError } variant='alternative'  type='submit'>
                        DEPLOY!
                    </CButton>}
                        <Box sx={{ mt: 3 }}>
                    {/* Messages */}    
                    {
                        state.isVerifying && (
                            <Message
                                title='Verfying on etherscan...'
                                variant='normal'
                                showCancel={false}
                            />
                        )
                    }
                    {
                        state.isDeploying && (
                            <Message
                                title='Deploying contract...'
                                variant='normal'
                                showCancel={false}
                            />
                        )
                    }
                    { deployError.deployError && 
                        (

                            <Message variant='error' title='The transaction was rejected' onCancel={ () => setDeployError({deployError: false})} /> 
                        )
                    }

                    { !isWalletConnected && <Message variant='error' title='Connect Wallet To Deploy!' onCancel={ () => setWalletError(false)} /> }
                    { props.chainError && <Message variant='error' title='Switch to Ethereum testnet to deploy :)' /> }

                        </Box>
                </Box>
                </form>
        </Grid>
        <Grid item xs={12} md={8}>
            <span className={classes.texts} >
                Deploy your own custom ERC721 contract here! An ERC721 contract allows you to mint 1/1 NFTs,
                not multiple editions. Choose your ticker and contract name carefully as they will be on the Ethereum
                blockchain forever<span>{String.fromCodePoint("0x1F631")}</span>. For more info <a href="https://gitlab.com/mimnermos/freemint/-/wikis/home">read the docs</a>.
            </span>
        </Grid>
        </Grid>
        <CModal title={'Success!'+String.fromCodePoint("0x1F973")+String.fromCodePoint("0x1F973")+String.fromCodePoint("0x1F973") } open={modal} close ={()=>setModal(false)}>
            <Box sx={{
                fontSize: '1rem',
                fontWeight: 600,
                mt: 6,
                wordWrap:'break-word'
            }}>
                Contract <i>{contract.contractAddress}</i> has been deployed! View it on <a href={"https://etherscan.io/tx/"+transactionHash.hash} target="_blank">Etherscan</a>. 
            </Box>
        </CModal>
      </Box>
    );
}


const useStyles = makeStyles({
    texts:{
        fontFamily:'Menlo',
        color:'black'
    },
    processMsg:{
        color:'black',
        fontFamily:'Menlo'
    },
})


export default DeployComponent;
