import Image1 from './Assets/images/1.png';
import Image2 from './Assets/images/2.png';
import Image3 from './Assets/images/3.png';
import Image4 from './Assets/images/4.png';
import Image5 from './Assets/images/5.png';
import Image6 from './Assets/images/6.png';
import Image7 from './Assets/images/7.png';

export {
    Image1,
    Image2,
    Image3,
    Image4,
    Image5,
    Image6,
    Image7,
}