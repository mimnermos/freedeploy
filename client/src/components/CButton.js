import React from "react";
import {Button} from '@mui/material'
import {makeStyles} from '@mui/styles'
import { grey } from '@mui/material/colors';

export const CButton = (props) =>{
    const { children, variant, ...rest } = props;
    const classes = useStyles();

    const variantStyles = {
        'normal': classes.normal,
        'alternate': classes.alternate
    }

    return (
        <Button
            className={`${classes.button} ${variantStyles[variant || 'normal']}`}
            {...rest}
        >
            {children}
        </Button>
    )
}

const useStyles = makeStyles((theme)=>({
    button: {
        '&.MuiButton-root':{
            background:'black',
            color:'white',
            fontFamily:'Menlo',
            fontSize:18,
            borderRadius: 10,
            boxShadow: '1px 1px #fff',
            '&:disabled': {
                opacity: 0.9,
                background: grey[900]
            },
            '&:hover':{
            background:'black',
            }
        },
    },
    normal:{
        '&.MuiButton-root':{
            color:theme.palette.primary.main,
            border:`3px solid ${theme.palette.primary.main}`,
        }
    },
    alternate:{
        '&.MuiButton-root':{
            color:'white',
        }
    } 
}))
