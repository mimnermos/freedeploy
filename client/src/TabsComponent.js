import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@mui/styles';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';

import DeployComponent from "./DeployComponent";
import MintComponent from "./MintComponent";
import AboutComponent from "./AboutComponent";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 0,
    minHeight:340,
    backgroundColor:theme.palette.primary.main,
    borderTopRightRadius: 10,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  tabView:{
    background:'#FFFFFF',
    width: '25rem',
    boxShadow: '5px 4px #fff',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    [theme.breakpoints.down('md')]: {
      width: '100%',
    },

  },
  tab: {
    '&.MuiTab-root':{
      color:'white',
      fontSize:18,
      fontFamily:'Menlo',
      backgroundColor:'black',
      padding:4,
      margin:3,
      borderTopLeftRadius: 10,
      borderTopRightRadius: 10,
    },
    '&.Mui-selected': {
      color:'black',
      fontSize:18,
      fontFamily:'Menlo',
      margin:2,
      backgroundColor:'inherit',
    },
    
  },
}));


function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`nav-tabpanel-${index}`}
      aria-labelledby={`nav-tab-${index}`}
      {...other}
    >
        <Box  hidden={value !== index} p={3}>
          <Typography component={'span'} >{children}</Typography>
        </Box>
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

const TabsComponent = (props) => {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  return (
    <Box >
      <Tabs
        className={classes.tabView}
        value={value}
        onChange={handleChange}
        textColor="secondary"
        variant='fullWidth'
      >
        <Tab className={classes.tab} label="Deploy"/>
        <Tab className={classes.tab} label="MINT"/>
      </Tabs>
      <Box className={classes.root}>
       <TabPanel  value={value} index={0}>
            <DeployComponent
                web3={props.web3}
                accounts={props.accounts}
                chainError={props.chainError}
             />
      </TabPanel>
      <TabPanel value={value} index={1}>
            <MintComponent
                web3={props.web3}
                accounts={props.accounts}
                chainError={props.chainError}
            />
      </TabPanel>
      </Box>
    </Box>
  );
}
export default TabsComponent;
