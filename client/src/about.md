### HOW TO DEPLOY

Click on the ‘deploy’ tab on wemint.art 

Choose your ticker and contract name, eg: ‘ART’ and ‘My Art Contract’. Choose the names carefully as they will be on the Ethereum blockchain forever.  

Click ‘deploy it!’. (will be ‘deploy!)

Check your contract has deployed by going to etherscan.io and entering your wallet address (this part will change as we’ll have a confirmation window). 

You have deployed your own customer ERC721 contract on the blockchain!

### HOW TO MINT

Get IPFS desktop or Pinata Cloud. 

Upload your artwork to IPFS.

Copy the CID of your artwork and paste it into your metadata .json file. 

The format of your metadata .json file should look something like this: 

{
    "name": "YOUR ARTWORK NAME",
    "description": "YOUR ARTWORK DESCRIPTION.",
    "image_url": "ipfs://YOUR ARTWORK CID"
}

See also [Opensea's documentation](https://docs.opensea.io/docs/metadata-standards)

Now upload your metadata file to IPFS.

Pin both your artwork and metadata file. 

On wemint.art, click on the ‘mint’ tab. 

Enter your contract address - you can find it on etherscan.io. 

Click ‘load contract’ (will be ‘load’) 

Enter your metadata URL – this is the CID of your metadata file with the prefix ‘ipfs://’ 

Click ‘Mint’ (will be ‘Mint!’)

After a couple of minutes you should be able to see your art on Opensea and Rarible!
